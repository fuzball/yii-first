<?php
$this->title = '更新缓存';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    
    <div class="col-xs-12 col-sm-12">
        <h4 class="red">
            <span class="middle"><i class="fa fa-exclamation-triangle light-red bigger-110" aria-hidden="true"></i></span>
            更新完成
        </h4>
        <div class="profile-user-info">
            <div class="profile-info-row">
                <div class="profile-info-value red">
                    <span>请记住您的操作，方便技术处理; 有任何技术或运营问题，请及时联系项目开发组</span>
                </div>
            </div>
        </div>
    </div>
</div>
