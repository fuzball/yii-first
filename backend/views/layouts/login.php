<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;

$asset = AppAsset::register($this);
?>
<?=$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
    <body class="login-layout">
    <?= $this->beginBody() ?>
<div class="main-container">
    <?=$content?>
</div>
    <?= $this->endBody() ?>
    </body>
</html>
<?= $this->endPage() ?>
