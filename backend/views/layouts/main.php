<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;

$asset = AppAsset::register($this);
$menu = $this->params['menus'];
//echo "<pre>";
//var_dump($menu);exit;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="no-skin">
<?php $this->beginBody() ?>

<!-- 顶部开始-->
<div id="navbar" class="navbar navbar-default ace-save-state">
    <!-- 顶部导航开始-->
    <div class="navbar-container ace-save-state" id="navbar-container">
        <div class="navbar-header pull-left">
            <a href="#" class="navbar-brand">
                <small>
                    <i class="fa fa-leaf"></i>
                    赞品优选全国代理系统
                </small>
            </a>
        </div>
        
        <!-- 顶部左侧LOGO结束 -->
        <div class="navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">

                <li class="green dropdown-modal">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="ace-icon fa fa-envelope icon-animated-vertical"></i>
                        <span class="badge badge-success">5</span>
                    </a>

                    <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                        <li class="dropdown-header">
                            <i class="ace-icon fa fa-envelope-o"></i> 5条未读消息
                        </li>

                        <li class="dropdown-content">
                            <ul class="dropdown-menu dropdown-navbar">
                                <li>
                                    <a href="#" class="clearfix">
                                        <img class="msg-photo" src="<?=$asset ->baseUrl?>/avatars/user.jpg" alt="Jason's Photo" />
                                        <span class="msg-body">
                                            <span class="msg-title">
                                                <span class="blue">系统:</span> 反射光栅
                                            </span>

                                            <span class="msg-time">
                                                <i class="ace-icon fa fa-clock-o"></i>
                                                <span>2017-01-08 23:20</span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="dropdown-menu dropdown-navbar">
                                <li>
                                    <a href="#" class="clearfix">
                                        <img class="msg-photo" src="<?=$asset->baseUrl?>/avatars/user.jpg" alt="Jason's Photo" />
                                        <span class="msg-body">
                                            <span class="msg-title">
                                                <span class="blue">系统:</span> 反射光栅asfafasfafafafasfafas
                                            </span>

                                            <span class="msg-time">
                                                <i class="ace-icon fa fa-clock-o"></i>
                                                <span>2017-01-08 23:20</span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="dropdown-footer">
                            <a href="#">
                                查看所有消息
                                <i class="ace-icon fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="light-blue dropdown-modal">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo" src="<?=!$this->params['user']->face ? $this->params['user']->face : $asset->baseUrl .'/avatars/user.jpg'?>" alt="Jason's Photo" />
                        <span class="user-info">
                            <small>欢迎光临,</small>
                            <?=$this->params['user']->username; ?>  
                        </span>
                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li><a href="<?=Url::toRoute(['site/upcache'])?>"><i class="ace-icon fa fa-flash"></i>更新缓存</a></li>
                        <li><a href="#"><i class="ace-icon fa fa-cog"></i>修改密码</a></li>
                        <li><a href="<?=Url::toRoute(['site/index'])?>"><i class="ace-icon fa fa-desktop"></i>登录信息</a></li>
                        <li><a href="<?=Url::toRoute(['admin/view'])?>"><i class="ace-icon fa fa-user"></i>个人信息</a></li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?=Url::toRoute(['site/logout'])?>" id="user-logout" data-method="post"><i class="ace-icon fa fa-power-off"></i>退出</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- 顶部右侧导航结束 -->
        </div>
    </div>
    <!-- 顶部导航结束 -->
</div>
<!-- 顶部结束 -->
<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>

<!-- #section开始 -->
<div id="sidebar" class="sidebar responsive ace-save-state" data-sidebar="true" data-sidebar-scroll="true" data-sidebar-hover="true">
    <script type="text/javascript">
        try {
            ace.settings.loadState('sidebar')
        } catch (e) {
        }
    </script>
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="ace-icon fa fa-signal"></i>
            </button>
            <button class="btn btn-info">
                <i class="ace-icon fa fa-pencil"></i>
            </button>
            <button class="btn btn-warning">
                <i class="ace-icon fa fa-users"></i>
            </button>
            <button class="btn btn-danger">
                <i class="ace-icon fa fa-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div>
    <!-- #sidebar-shortcuts -->
    <!-- 左侧菜单开始 -->
    <ul class="nav nav-list">
        <li class="">
            <a href="#">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> 后台首页 </span>
            </a>
            <b class="arrow"></b>
        </li>
        <?php  foreach ($this->params['menus'] as $value) { 
    
            ?>
            <li>
        <a <?php if ($value['pid'] == 0 && ! empty($value['child'])) { ?> class="dropdown-toggle" <?php } ?> href="<?php echo !empty($value['url']) ? Url::to([$value['url']]) : '#'; ?>">
                    <i class="menu-icon fa <?=$value['icons']?>"></i>
                    <span class="menu-text"> <?=$value['menu_name']?> </span>
                    <?php if ($value['pid'] == 0 && ! empty($value['child'])){ ?>
                        <b class="arrow fa fa-angle-down"></b>
                    <?php }?>
                </a>
                <?php if ($value['pid'] == 0 && ! empty($value['child'])) { ?>
                    <ul class="submenu">
                        <?php foreach ($value['child'] as $val) {?>
                            <li>
                                <a href="<?=Url::toRoute([$val['url']])?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    <?=$val['menu_name']?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <!-- 左侧菜单结束 -->
    <!-- 左侧隐藏开始 -->
    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
    
    <!-- 左侧隐藏结束 -->
</div>
<!-- #section结束 -->

<!--主要内容信息-->
<div class="main-content">

    <!--头部可固定导航信息-->
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <!--面包屑信息-->
        <ul class="breadcrumb">
            <?= Breadcrumbs::widget(
                [
                    'homeLink' => [
                        'label' => '<i class="ace-icon fa fa-home home-icon"></i> 首页',
                        'url' => ['/']
                    ],
                    'encodeLabels' => false,
                    'tag' => 'ol',
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
                ]
            ); ?>
        </ul>

        <!--搜索-->
        <div class="nav-search" id="nav-search">
            <form class="form-search">
                <span class="input-icon">
                    <input type="text" placeholder="搜索信息" class="nav-search-input" id="nav-search-input" autocomplete="on" />
                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                </span>
            </form>
        </div>
    </div>

    <div class="page-content">

        <!--样式设置信息-->
        <div class="ace-settings-container" id="ace-settings-container">
            <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                <i class="ace-icon fa fa-cog bigger-150"></i>
            </div>


            <div class="ace-settings-box clearfix" id="ace-settings-box">
                <div class="pull-left width-50">
                    <div class="ace-settings-item">
                        <div class="pull-left">
                            <select id="skin-colorpicker" class="hide">
                                <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                            </select>
                        </div>
                        <span>&nbsp; 选择皮肤 </span>
                    </div>

                    <div class="ace-settings-item">
                        <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
                        <label class="lbl" for="ace-settings-navbar"> 固定导航栏 </label>
                    </div>

                    <div class="ace-settings-item">
                        <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
                        <label class="lbl" for="ace-settings-sidebar"> 固定侧边栏 </label>
                    </div>
                </div>
            </div>
        </div>

        <!--主要内容信息-->
        <div class="page-content-area">
            <div class="page-header">
                <h1> <?=$this->title;?>
<!--                        <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        编辑我的信息
                    </small>-->
                </h1>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?= $content ?>
                </div>
            </div>
        </div>

    </div>
</div>
<!--内容结束-->

<!--底部内容开始-->
<div class="footer">
        <div class="footer-inner">
            <div class="footer-content">
                <span class="bigger-120">
                   <span class="red bolder">技术支持:赞品优选项目组</span>
                    赞品优选全国代理系统 © 2016-2017
                </span>
            </div>
        </div>
    </div>
<!--底部内容结束-->
</div>
<?php $this->endBody() ?>
<script type="text/javascript">
    $(function(){
        // 导航栏样式装换
        var select = 'ul.nav-list a[href=' + window.location.pathname.replace(/\//g, '\\/') +']';
        console.log(select);
        $(select).closest('li').addClass('active').parentsUntil('ul.nav-list').addClass('active open');
    });
</script>
</body>
</html>
<?php $this->endPage() ?>
