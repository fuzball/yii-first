<?php
namespace backend\controllers;


use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\helpers\Helper;
use backend\models\Admin;
use backend\models\AdminForm;
use backend\models\Menu;
/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','upcache'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * 更新菜单缓存
     *
     * @return string
     */
    public function actionUpcache(){
        Menu::setParentMenus(Yii::$app->user->id);
        return $this->render('upcache');
    }
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

/*        // 获取用户导航栏信息
        $menus = Menu::getUserMenus(Yii::$app->user->id);
        if ($menus) {
            // 用户信息和导航栏目
            Yii::$app->view->params['menus'] = $menus;
            Yii::$app->view->params['user']  = Yii::$app->getUser()->identity;

            // 系统信息
            $system = explode(' ', php_uname());
            $system = $system[0] .'&nbsp;' . ('/' == DIRECTORY_SEPARATOR ? $system[2] : $system[1]);

            // MySql版本
            $version = Yii::$app->db->createCommand('SELECT VERSION() AS `version`')->queryOne();

            // 加载视图
            return $this->render('index', [
                'system' => $system,                                        // 系统信息
                'yii'    => 'Yii '. Yii::getVersion(),                      // Yii 版本
                'php'    => 'PHP '. PHP_VERSION,                            // PHP 版本
                'server' => $_SERVER['SERVER_SOFTWARE'],                    // 服务器信息
                'mysql'  => 'MySQL '.($version ? $version['version'] : ''), // Mysql版本
                'upload' => ini_get('upload_max_filesize'),                 // 上传文件大小
            ]);
        } else {
            throw new UnauthorizedHttpException('对不起，您还没获得显示导航栏目权限!');
        }*/

        // 系统信息
        $system = explode(' ', php_uname());
        $system = $system[0] . '&nbsp;' . ('/' == DIRECTORY_SEPARATOR ? $system[2] : $system[1]);
        // MySql版本
        $version = Yii::$app->db->createCommand('SELECT VERSION() AS `version`')->queryOne();
        return $this->render('index', [
                    'system' => $system, // 系统信息
                    'yii' => 'Yii ' . Yii::getVersion(), // Yii 版本
                    'php' => 'PHP ' . PHP_VERSION, // PHP 版本
                    'server' => $_SERVER['SERVER_SOFTWARE'], // 服务器信息
                    'mysql' => 'MySQL ' . ($version ? $version['version'] : ''), // Mysql版本
                    'upload' => ini_get('upload_max_filesize'), // 上传文件大小
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new AdminForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        } else {

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        // 用户退出修改登录时间
        $admin = Admin::findOne(Yii::$app->user->id);
        if ($admin) {
            $admin->last_time = time();
            $admin->last_ip   = Helper::getIpAddress();
            $r = $admin->save();
        }
        Yii::$app->user->logout();
        Yii::$app->cache->flush();
        return $this->goHome();
    }
    
}
