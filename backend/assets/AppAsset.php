<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/public/';
    public static $baseimgUrl = '@web/public/';
    //加载CSS
    public $css = [
        'css/bootstrap.css',
        'components/font-awesome/css/font-awesome.css',
        'css/ace-fonts.css',
        'css/ace.css',
        'css/ace-skins.css',
        'css/ace-rtl.css',
    ];
    
    //加载JS
    public $js = [
        //'components/jquery/dist/jquery.js',//v2.2.1
        'components/bootstrap/dist/js/bootstrap.js',
        'components/_mod/jquery-ui.custom/jquery-ui.custom.js',//jQuery UI插件
        'components/jqueryui-touch-punch/jquery.ui.touch-punch.js',//触摸事件
        'js/ace-extra.js',
        'js/ace-elements.js',
        'js/ace.js',
        //'components/_mod/easypiechart/jquery.easypiechart.js',//轻量级的jQuery插件，用来渲染和制作漂亮的饼图及动画效果，基于与HTML5的canvas元素
        //'components/jquery.sparkline/index.js',//开源的图表控件，可以生成很小也很好看的图
        
    ];

    //加载
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    
    /**
     * ------------------------------------------
     * 定义按需加载JS方法，注意加载顺序在最后
     * @param $view \yii\web\View
     * @param $jsfile string
     * ------------------------------------------
     */
    public static function addScript($view, $jsfile) {
        $view->registerJsFile($jsfile, [AppAsset::className(), 'depends' => 'assets\AppAsset']);
    }
    /**
     * ------------------------------------------
     * 定义按需加载css方法，注意加载顺序在最后
     * @param $view \yii\web\View
     * @param $cssfile string
     * ------------------------------------------
     */
    public static function addCss($view, $cssfile) {
        $view->registerCssFile($cssfile, [AppAsset::className(), 'depends' => 'assets\AppAsset']);
    }
}
